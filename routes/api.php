<?php

/*
  Created on     : Sep 5, 2018 10:19:13 AM
  Author       : El Makong <albert.abimanyu@gmail.com>
 */

//remote api
use Illuminate\Database\Capsule\Manager as Capsule;
use Slim\Http\Request;
use Slim\Http\Response;

global $app;

$app->group('/api', function () {
    $this->get('/rc', function(Request $request, Response $response, array $args) {
        // returns data to be checked by citizenship ministry
        $data = Capsule::table('submissions')->where('checkedAt', NULL)->select('nik', 'name')->get();
//        $data = [
//            [
//                'a' => 'nama saya',
//                'n' => '123456789'
//            ],
//            [
//                'a' => 'nama orang',
//                'n' => '987654321'
//            ]
//        ];
        return $response->withJson($data);
    });
});
