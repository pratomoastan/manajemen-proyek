<html>
    <head>
        <meta charset="UTF-8">
        <title>{$pagetitle}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/popper.min.js"></script>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <script src="../assets/js/bootstrap.min.js"></script>
        <!-- end Bootstrap -->

      
    </head>
    <body>
        <div class="container-fluid">
        {*{if isset($alert)}
            <div class="alert">{$alert}</div>
        {/if}*}
        {if isset($alert)}
                <div class="alert alert-warning alert-dismissible fade show col-sm-2" role="alert">
                    {$alert}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        {/if}