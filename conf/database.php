<?php

/*
  Created on     : Feb 25, 2018 3:10:16 PM
  Author       : El Makong <albert.abimanyu@gmail.com>
 */

/* Start Database */

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => $db_host,
    'database' => $db_name,
    'username' => $db_username,
    'password' => $db_password,
    'charset' => $db_charset,
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

//// Set the event dispatcher used by Eloquent models... (optional)
//use Illuminate\Events\Dispatcher;
//use Illuminate\Container\Container;
//
//$capsule->setEventDispatcher(new Dispatcher(new Container));
// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

/* End Database */