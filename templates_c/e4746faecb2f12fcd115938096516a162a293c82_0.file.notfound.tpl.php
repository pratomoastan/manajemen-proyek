<?php
/* Smarty version 3.1.32, created on 2018-09-21 04:29:37
  from 'D:\xampp\htdocs\mapro\templates\default\errors\notfound.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5ba457914d3108_39180536',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e4746faecb2f12fcd115938096516a162a293c82' => 
    array (
      0 => 'D:\\xampp\\htdocs\\mapro\\templates\\default\\errors\\notfound.tpl',
      1 => 1536113661,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba457914d3108_39180536 (Smarty_Internal_Template $_smarty_tpl) {
?><head>
    <title>Page Not Found</title>
    <style>
        body{
            margin:0;
            padding:30px;
            font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;
        }
        h1{
            margin:0;
            font-size:48px;
            font-weight:normal;
            line-height:48px;
        }
        strong{
            display:inline-block;
            width:65px;
        }
    </style>
</head>
<body>
    <h1>Page Not Found</h1>
    <p>
        The page you are looking for could not be found. Check the address bar
        to ensure your URL is spelled correctly. If all else fails, you can
        visit our home page at the link below.
    </p>
    <a href="/">Visit the Home Page</a>

</body><?php }
}
