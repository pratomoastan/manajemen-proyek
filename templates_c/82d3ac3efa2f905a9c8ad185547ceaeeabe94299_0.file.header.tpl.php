<?php
/* Smarty version 3.1.32, created on 2018-09-19 04:32:19
  from 'C:\xampp\htdocs\mapro\templates\default\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5ba1b533595c07_34693142',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82d3ac3efa2f905a9c8ad185547ceaeeabe94299' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mapro\\templates\\default\\header.tpl',
      1 => 1537324304,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba1b533595c07_34693142 (Smarty_Internal_Template $_smarty_tpl) {
?><html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $_smarty_tpl->tpl_vars['pagetitle']->value;?>
</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="assets/css/login.css">
        <!-- Bootstrap -->
        <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"><?php echo '</script'; ?>
>
        <!-- end Bootstrap -->

        <noscript>
        <?php echo alertFluid('danger text-center rounded-0',$_smarty_tpl->tpl_vars['_LANG']->value['javascriptdisabled']);?>

        </noscript>
    </head>
    <body>
        <?php if (isset($_smarty_tpl->tpl_vars['alert']->value)) {?>
            <div class="alert"><?php echo $_smarty_tpl->tpl_vars['alert']->value;?>
</div>
        <?php }
}
}
