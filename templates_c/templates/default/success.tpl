{include file="header.tpl"}
<div class="pretext">
    <!-- data shown after printing prompt, wont appear in printed document -->
    Your data has been submitted.<br>
    Data:
</div>
<div class="data">
    Nama: {$userdata->name}<br>
    NIK: {$userdata->nik}<br>
</div>
<div class="posttext" style="display: none;">
    <!-- data shown on print bottom -->
    Thank you for using our service
</div>
<div class="pretext">
    <a class="btn btn-primary" href="/">Home</a>
</div>
<script src="/assets/js/submitsuccess.js"></script>
{include file="footer.tpl"}