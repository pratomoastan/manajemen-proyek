<?php
/* Smarty version 3.1.32, created on 2018-09-05 11:50:52
  from 'C:\xampp\htdocs\mapro\templates\default\success.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b8f60ac2cd8a2_42067220',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b5675512bb9263b5aba2123ea3cd231c890682e9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mapro\\templates\\default\\success.tpl',
      1 => 1536123040,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5b8f60ac2cd8a2_42067220 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<div class="pretext">
    <!-- data shown after printing prompt, wont appear in printed document -->
    Your data has been submitted.<br>
    Data:
</div>
<div class="data">
    Nama: <?php echo $_smarty_tpl->tpl_vars['userdata']->value->name;?>
<br>
    NIK: <?php echo $_smarty_tpl->tpl_vars['userdata']->value->nik;?>
<br>
</div>
<div class="posttext" style="display: none;">
    <!-- data shown on print bottom -->
    Thank you for using our service
</div>
<div class="pretext">
    <a href="/">Home</a>
</div>
<?php echo '<script'; ?>
 src="/assets/js/submitsuccess.js"><?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
