<?php

/*
  Created on     : Sep 5, 2018 10:29:09 AM
  Author       : El Makong <albert.abimanyu@gmail.com>
 */

use Illuminate\Database\Capsule\Manager as Capsule;
use Slim\Http\Request;
use Slim\Http\Response;

global $app;

$app->group('', function() {
    $this->group('/login', function() {
        $this->get('', function(Request $request, Response $response, array $args) {
            return $this->view->render($response, 'form/login.tpl', [
                        'curroute' => $request->getUri()->getPath(),
                        'pagetitle' => 'Validasi KTP',
                        'alert' => $this->flash->getFirstMessage('alert')
            ]);
        })->setName('login');
        $this->post('', function(Request $request, Response $response, array $args) {
            if ($request->getParam('un') == 'user' && $request->getParam('pw') == 'user') {
                //Login ok
                $_SESSION['ktpses'] = 'user';
                $this->flash->addMessage('alert', 'Login Berhasil');
                if (isset($_SESSION['redirect'])) {
                    $redirect = $_SESSION['redirect'];
                    unset($_SESSION['redirect']);
                    return $response->withRedirect($redirect);
                } else {
                    $url = $this->router->pathFor('home');
                    return $response->withRedirect($url);
                }
            } else {
                //login failed
                $this->flash->addMessage('alert', 'Login Gagal');
                $url = $this->router->pathFor('home');
                return $response->withRedirect($url);
            }
        });
    });
    $this->get('/logout', function(Request $request, Response $response, array $args) {
        session_unset();
        session_destroy();
        $url = $this->router->pathFor('login');
        return $response->withRedirect($url);
    });
});
