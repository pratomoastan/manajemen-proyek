<?php
/* Smarty version 3.1.32, created on 2018-09-26 07:04:06
  from 'D:\xampp\htdocs\mapro\templates\default\success.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bab1346080783_99709782',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd0ded778d634d81c07afa047a85991bd3bff48b8' => 
    array (
      0 => 'D:\\xampp\\htdocs\\mapro\\templates\\default\\success.tpl',
      1 => 1537938228,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:navbar.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5bab1346080783_99709782 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:navbar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<br/><br/><br/><br/>
<div class="container text-center">
    <div class="pretext">
        <!-- data shown after printing prompt, wont appear in printed document -->
        <div class="alert alert-success" role="alert">
            Your data has been submitted.
        </div>
        <br>
        Data:
    </div>
    <div class="data">
        
        Nama: <?php echo $_smarty_tpl->tpl_vars['userdata']->value->name;?>
<br>
        NIK: <?php echo $_smarty_tpl->tpl_vars['userdata']->value->nik;?>
<br>
    </div>
    <div class="posttext" style="display: none;">
        <!-- data shown on print bottom -->
        Thank you for using our service
    </div>
    <br/><br/>
    <div class="pretext">
        <a class="b btn btn-outline-success btn-success" href="/">Home</a>
    </div>
</div>
<?php echo '<script'; ?>
 src="/assets/js/submitsuccess.js"><?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
