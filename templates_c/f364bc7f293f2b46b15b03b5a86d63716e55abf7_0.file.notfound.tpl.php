<?php
/* Smarty version 3.1.32, created on 2018-09-05 09:15:41
  from 'C:\xampp\htdocs\mapro\templates\default\errors\notfound.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b8f3c4dcc7855_89789798',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f364bc7f293f2b46b15b03b5a86d63716e55abf7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mapro\\templates\\default\\errors\\notfound.tpl',
      1 => 1536113661,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b8f3c4dcc7855_89789798 (Smarty_Internal_Template $_smarty_tpl) {
?><head>
    <title>Page Not Found</title>
    <style>
        body{
            margin:0;
            padding:30px;
            font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;
        }
        h1{
            margin:0;
            font-size:48px;
            font-weight:normal;
            line-height:48px;
        }
        strong{
            display:inline-block;
            width:65px;
        }
    </style>
</head>
<body>
    <h1>Page Not Found</h1>
    <p>
        The page you are looking for could not be found. Check the address bar
        to ensure your URL is spelled correctly. If all else fails, you can
        visit our home page at the link below.
    </p>
    <a href="/">Visit the Home Page</a>

</body><?php }
}
