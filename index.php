<?php

require 'vendor/autoload.php';
require_once 'conf/config.php';
require_once 'conf/database.php';
require_once 'conf/utils.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Slim\Http\Request;
use Slim\Http\Response;

session_start();
$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$app = new \Slim\App(['settings' => $config]);
$app->add(function ($request, $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));

        if ($request->getMethod() == 'GET') {
            return $response->withRedirect((string) $uri, 301);
        } else {
            return $next($request->withUri($uri), $response);
        }
    }

    return $next($request, $response);
});

$container = $app->getContainer();

$container['view'] = function ($c) {
    global $systpl;
    $view = new \Slim\Views\Smarty('templates/' . $systpl, [
        //      'cacheDir'=>'/cache',
        'compileDir' => 'templates_c/'
    ]);
    $smartyPlugins = new Slim\Views\SmartyPlugins($c['router'], $c['request']->getUri());
    $view->registerPlugin('function', 'path_for', [$smartyPlugins, 'pathFor']);
    $view->registerPlugin('function', 'base_url', [$smartyPlugins, 'baseUrl']);

    return $view;
};
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['view']->render($response, 'errors/notallowed.tpl', [
                    'curroute' => $request->getUri()->getPath(),
                    'methods' => $methods
                ])->withStatus(405)->withHeader('Content-Type', 'text/html');
    };
};
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['view']->render($response, 'errors/notfound.tpl', [
                    'curroute' => $request->getUri()->getPath()
                ])->withStatus(404)->withHeader('Content-Type', 'text/html');
    };
};

require_all('routes/');
$app->run();
?>