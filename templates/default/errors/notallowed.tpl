<head>
    <title>Method not allowed</title>
    <style> 
        body{ margin:0; padding:30px; font:12px/1.5 Helvetica,Arial,Verdana,sans-serif; } 
        h1{ margin:0; font-size:48px; font-weight:normal; line-height:48px; }
    </style>
</head>
<body>
    <h1>Method not allowed</h1>
    <p>Method not allowed. Must be one of: <strong>{','|implode:$methods}</strong></p>
    <p>Back to <a href="/">home</a>.</p>
</body>