
/*
 Created on     : Sep 5, 2018 11:37:19 AM
 Author       : El Makong <albert.abimanyu@gmail.com>
 */

$(document).ready(function () {
    tgltxt();
    window.print();
    window.onafterprint = function () {
        tgltxt();
    };
});
function tgltxt() {
    // set up hidden/shown div elements here
    $('.alert').toggle();
    $('.pretext').toggle();
    $('.posttext').toggle();
}
