{include file="header.tpl"}
{include file="navbar.tpl"}
<br/><br/><br/><br/>
<div class="container text-center">
    <div class="pretext">
        <!-- data shown after printing prompt, wont appear in printed document -->
        <div class="alert alert-success" role="alert">
            Your data has been submitted.
        </div>
        <br>
        Data:
    </div>
    <div class="data">
        {*<input class="form-control" type="text" placeholder="Readonly input here…" readonly>
        <input class="form-control" type="text" placeholder="Readonly input here…" readonly>*}

        Nama: {$userdata->name}<br>
        NIK: {$userdata->nik}<br>
    </div>
    <div class="posttext" style="display: none;">
        <!-- data shown on print bottom -->
        Thank you for using our service
    </div>
    <br/><br/>
    <div class="pretext">
        <a class="b btn btn-outline-success btn-success" href="/">Home</a>
    </div>
</div>
<script src="/assets/js/submitsuccess.js"></script>
{include file="footer.tpl"}