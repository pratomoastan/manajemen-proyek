<?php
/* Smarty version 3.1.32, created on 2018-09-05 09:29:06
  from 'C:\xampp\htdocs\mapro\templates\default\errors\notallowed.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b8f3f72c70ff7_36547865',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'be926531f4d5720d91f8b5699cbd623f089d234a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mapro\\templates\\default\\errors\\notallowed.tpl',
      1 => 1536113661,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b8f3f72c70ff7_36547865 (Smarty_Internal_Template $_smarty_tpl) {
?><head>
    <title>Method not allowed</title>
    <style> 
        body{ margin:0; padding:30px; font:12px/1.5 Helvetica,Arial,Verdana,sans-serif; } 
        h1{ margin:0; font-size:48px; font-weight:normal; line-height:48px; }
    </style>
</head>
<body>
    <h1>Method not allowed</h1>
    <p>Method not allowed. Must be one of: <strong><?php echo implode(',',$_smarty_tpl->tpl_vars['methods']->value);?>
</strong></p>
    <p>Back to <a href="/">home</a>.</p>
</body><?php }
}
