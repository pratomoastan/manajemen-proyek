<?php

/*
  Created on     : Sep 5, 2018 10:25:26 AM
  Author       : El Makong <albert.abimanyu@gmail.com>
 */

use Illuminate\Database\Capsule\Manager as Capsule;
use Slim\Http\Request;
use Slim\Http\Response;

global $app;

$app->group('', function() {
    $this->get('/', function(Request $request, Response $response, array $args) {
        return $this->view->render($response, 'form/homeform.tpl', [
                    'curroute' => $request->getUri()->getPath(),
                    'pagetitle' => 'Validasi KTP',
                    'alert' => $this->flash->getFirstMessage('alert')
        ]);
    })->setName('home');
    $this->post('/submit', function(Request $request, Response $response, array $args) {
    ///add data
    //        echo var_dump($_POST);
    //    echo $request->getParam('nik');
        $exists = Capsule::table('submissions')->where('nik', $request->getParam('nik'))->exists();
        $url = $this->router->pathFor('home');
        if (!$exists) {
            //add to db
            Capsule::table('submissions')->insert([
                'nik' => $request->getParam('nik'),
                'name' => $request->getParam('name'),
                'email' => $request->getParam('email'),
                'ip' => inet_pton($_SERVER['REMOTE_ADDR'])
            ]);
            $this->flash->addMessage('alert', 'Input Sukses');
            $url = $this->router->pathFor('success', ['nik' => $request->getParam('nik')]);
        }else{
            $this->flash->addMessage('alert', 'NIK sudah ada');
            $url = $this->router->pathFor('home', ['nik' => $request->getParam('nik')]);
        }
        return $response->withRedirect($url);
    });
    $this->get('/success/{nik}', function(Request $request, Response $response, array $args) {
        return $this->view->render($response, 'success.tpl', [
                    'curroute' => $request->getUri()->getPath(),
                    'pagetitle' => 'Validasi KTP',
                    'alert' => $this->flash->getFirstMessage('alert'),
                    'userdata' => Capsule::table('submissions')->where('nik', $args['nik'])->first()
        ]);
    })->setName('success');
})->add(function (Request $request, $response, $next) {
    if (!isset($_SESSION['ktpses'])) {
        $_SESSION['redirect'] = $request->getUri()->getPath();
        $url = $this->router->pathFor('login');
        return $response->withRedirect($url);
    } else {
        $response = $next($request, $response);
        return $response;
    }
});
