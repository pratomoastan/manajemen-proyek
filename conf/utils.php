<?php

/*
  Created on     : Nov 21, 2017 8:49:03 PM
  Author       : El Makong <albert.abimanyu@gmail.com>
 */

/**
 * Assign smarty variable "alert" on header.tpl
 * @param String $type primary, secondary, success, danger, warning, info, light, dark
 * @param String $type can be styled. Using spaces as separator. The type must be typed in first. ex: "primary rounded-0"
 * @param String $message Message content
 * @param Boolean $dismissable Set to TRUE if alert is dismissable.
 * @example alert('success','message',TRUE)
 * @example alert('success rounded-0','message')
 * @see https://getbootstrap.com/docs/4.0/components/alerts/ $Type definition
 *
 */
function alert($type, $message, $dismissable = NULL) {
//    global $smarty;
    $type = strtolower($type);

    $msg = '    <div class="container alert alert-' . $type . ' fade show">';
    if ($dismissable == TRUE) {
        $msg .= '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
    }
    $msg .= $message . '</div>';
//    $smarty->assign('alert', $msg);
    return $msg;
}

/**
 * The full width version of alert.
 * Used on dashboard notify
 *
 */
function alertFluid($type, $message, $dismissable = NULL) {
//    global $smarty;
    $type = strtolower($type);

    $msg = '    <div class="container-fluid alert alert-' . $type . ' fade show py-1 m-0">';
    if ($dismissable == TRUE) {
        $msg .= '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
    }
    $msg .= $message . '</div>';
//    $smarty->assign('alert', $msg);
    return $msg;
}

/**
 * Scan the include file path, recursively including all PHP files
 *
 * @param string  $dir
 * @param int     $depth (optional)
 */
function require_all($dir, $depth = 0, $maxScanDepth = 4) {
    if ($depth > $maxScanDepth) {
        return;
    }
    // require all php files
    $scan = glob("$dir" . DIRECTORY_SEPARATOR . "*");
    foreach ($scan as $path) {
        if (preg_match('/\.php$/', $path)) {
            require_once $path;
//            include_once $path;
        } elseif (is_dir($path)) {
            require_all($path, $depth + 1);
        }
    }
}
